# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php8-pecl-protobuf
_extname=protobuf
pkgver=3.19.0
pkgrel=0
pkgdesc="PHP 8 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data - PECL"
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
depends="php8-common"
makedepends="php8-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize8
	./configure --prefix=/usr --with-php-config=php-config8
	make
}

check() {
	# Test suite is not a part of pecl release.
	php8 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php8/conf.d
	install -d $_confdir
	echo "extension=$_extname.so" > $_confdir/$_extname.ini
}

sha512sums="
1483c18f81968c9fd4722c3b6ac6586a6f52c27b3bb42d9b57ef383c5fb7f6f0107ff81b3cdce6bff8b9f92305b405e9c1c0d2d965ec276c1bd2a8adf7a62b26  php-pecl-protobuf-3.19.0.tgz
"
