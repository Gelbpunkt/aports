# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=chezmoi
pkgver=2.7.1
pkgrel=0
pkgdesc="Manage your dotfiles across multiple machines, securely."
url="https://www.chezmoi.io/"
arch="all"
license="MIT"
makedepends="go"
options="!check chmod-clean" # no test suite
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/twpayne/chezmoi/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	go build \
		-ldflags "
		-X \"main.version=$pkgver\" \
		-X \"main.date=$(date --utc +%Y-%m-%dT%H:%M:%SZ)\" \
		-X \"github.com/twpayne/chezmoi/cmd.DocsDir=/usr/share/doc/chezmoi/\"
		" \
		-tags noupgrade \
		-tags noembeddocs
	make completions
}

package() {
	install -Dm0755 chezmoi "$pkgdir"/usr/bin/chezmoi
	install -Dm0644 completions/chezmoi-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm0644 completions/chezmoi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
	install -Dm0644 completions/chezmoi.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc/chezmoi"
	cp "$builddir/docs/"* "$subpkgdir/usr/share/doc/chezmoi"
}

sha512sums="
7a7f9b849f672d6cfc26b23ab0fa5084eaa228d54f828c80a0ada8f4980fd31e7ecee4a5e18ddd62249bacc3ddc0e0d34fc1209d6424106eb453b8b751ac2c77  chezmoi-2.7.1.tar.gz
"
